from settings import *


def prepare_print(currency_json, key, usd_to_local_rate):
    values = {}
    for currency in currency_json:
        if currency['id'] == key:
            currency_balance = currencies[key]["balance"]
            price_usd = float(currency['price_usd'])
            price_local = price_usd * usd_to_local_rate
            price_local_str = "{:.3f}".format(price_local)
            balance_in_local = currency_balance*price_local
            balance_in_local_str = "{:.3f}".format(balance_in_local)
            values['currency'] = key
            values['balance'] = balance_in_local_str
            values['unit_in_local'] = price_local_str
            if "1h" in percentaje_change_interval:
                values['1h'] = currency["percent_change_1h"]
            if "24h" in percentaje_change_interval:
                values['24h'] = currency["percent_change_24h"]
            if "7d" in percentaje_change_interval:
                values['7d'] = currency["percent_change_7d"]
            break
    return values


def prepare_total(prepared_values):
    values = {"currency": "total", "balance": 0.0, "unit_in_local": None}
    total_balance = 0.0
    one_hour = 0.0
    twenty_four_hour = 0.0
    seven_days = 0.0
    for i, row in enumerate(prepared_values):
        total_balance += float(row['balance'])
        weight = float(row['balance']) / total_balance
        if "1h" in percentaje_change_interval:
            one_hour += (weight*float(row['1h']))
        if "24h" in percentaje_change_interval:
            twenty_four_hour += (weight * float(row['24h']))
        if "7d" in percentaje_change_interval:
            seven_days += (weight*float(row['7d']))
    values["balance"] = total_balance
    if one_hour:
        values['1h'] = "{:.3f}".format(one_hour)
    if twenty_four_hour:
        values['24h'] = "{:.3f}".format(twenty_four_hour)
    if seven_days:
        values['7d'] = "{:.3f}".format(seven_days)
    return values
