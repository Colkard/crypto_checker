# coding=utf-8
import requests
import sys
from settings import *
from PrintHelper import *
from tabulate import tabulate


def get_matrix_from_array_dict(tabulate_values):
    for i, tabulate_row in enumerate(tabulate_values):
        aux = [tabulate_row['currency'], tabulate_row['balance'], tabulate_row['unit_in_local']]
        if "1h" in tabulate_row:
            aux.append(tabulate_row['1h'])
        if "24h" in tabulate_row:
            aux.append(tabulate_row['24h'])
        if "7d" in tabulate_row:
            aux.append(tabulate_row['7d'])
        tabulate_values[i] = aux


def append_base_values_to_percentages(tabulate_values):
    pass

tabulate_values = []
tabulate_headers = ['Currency', 'Balance({})'.format(local_currency), 'Unit({})'.format(local_currency)]
if "1h" in percentaje_change_interval:
    tabulate_headers.append("1h %")
if "24h" in percentaje_change_interval:
    tabulate_headers.append("24h %")
if "7d" in percentaje_change_interval:
    tabulate_headers.append("7d %")

my_currencies_json = requests.get("https://api.coinmarketcap.com/v1/ticker").json()
usd_to_local_rate = 1.0
if local_currency != "USD":
    usd_to_local_rate = float(requests.get("https://api.fixer.io/latest?base=USD").json()["rates"][local_currency])

if sys.version_info >= (3, 0):
    for key, value in currencies.items():
        if value["show"]:
            prepared_values = prepare_print(my_currencies_json, key, usd_to_local_rate)
            if prepared_values:
                tabulate_values.append(prepared_values)
else:
    for key, value in currencies.iteritems():
        if value["show"]:
            prepared_values = prepare_print(my_currencies_json, key, usd_to_local_rate)
            if prepared_values:
                tabulate_values.append(prepared_values)

tabulate_values.append(prepare_total(tabulate_values))
get_matrix_from_array_dict(tabulate_values)
tabulate_values = sorted(tabulate_values, key=lambda x: (x[0] == "total", x[0]))
append_base_values_to_percentages(tabulate_values)

print(tabulate(tabulate_values, headers=tabulate_headers, tablefmt="fancy_grid"))
